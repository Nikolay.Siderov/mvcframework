using System;
using Xunit;

namespace WebFramework.MvcFramework.Tests
{
    using System.Collections.Generic;
    using System.IO;
    using WebFramework.MvcFramework;

    public class ViewEngineTests
    {
        [Theory]
        [InlineData("CleanHtml")]
        [InlineData("Foreach")]
        [InlineData("IfElseFor")]
        [InlineData("ViewModel")]
        public void Test1(string fileName)
        {
            var viewModel = new TestViewModel
            {
                DateOfBirth = new DateTime(2019, 6, 1),
                Name = "Ivan",
                Price = 364.31M
            };
            IViewEngine viewEngine = new ViewEngine();
            var view = File.ReadAllText($"ViewTests/{fileName}.html");
            var result = viewEngine.GetHtml(view, viewModel);
            var expectedResult = File.ReadAllText($"ViewTests/{fileName}.Result.html");
            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void TemplateViewModel()
        {
            IViewEngine viewEngine = new ViewEngine();

            var actualResult = viewEngine.GetHtml(@"@foreach(var num in Model)
{
<span>@num</span>
}", new List<int> { 1, 2, 3 });

            var expectedResul = @"<span>1</span>
<span>2</span>
<span>3</span>
";
            Assert.Equal(expectedResul, actualResult);
        }
    }
}
