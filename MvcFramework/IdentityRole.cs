﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFramework.MvcFramework
{
    public enum IdentityRole
    {
        User = 1,
        Admin = 2,
    }
}
