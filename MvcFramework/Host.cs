﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebFramework.MvcFramework
{
    using System;
    using System.Linq;
    using System.Reflection;
    using WebFramework.HttpServer;
    public static class Host
    {
        public static async Task StartUpAsync(IMvcApplication application, int port)
        {
            var server = new HttpServer();
            IServiceCollection serviceCollection = new ServiceCollection();

            application.ConfigureServices(serviceCollection);
            application.Configure(server);

            AutoRegisterRoutes(server, application, serviceCollection);
            await server.StartAsync(port);
        }

        private static void AutoRegisterRoutes(IRouter router, IMvcApplication application, IServiceCollection serviceCollection)
        {
           // Gets all controllers in the assembly
            var controllers = application.GetType().Assembly.GetTypes()
                .Where(x => x.IsClass && !x.IsAbstract && x.IsSubclassOf(typeof(Controller)));
             
            foreach (var controller in controllers)
            {
                var methods = controller.GetMethods()
                    .Where(x => 
                    x.IsPublic && !x.IsStatic &&
                    !x.IsAbstract && !x.IsConstructor &&
                    !x.IsSpecialName && // removes all getters and setters
                    x.DeclaringType == controller); // Only the methods declared in the current class

                foreach (var method in methods)
                {
                    var attribute = method.GetCustomAttributes(false)
                        .Where(x => x.GetType().IsSubclassOf(typeof(BaseHttpAttribute)))
                        .FirstOrDefault() as BaseHttpAttribute;

                    string url = attribute?.Url ?? ("/" + controller.Name.Replace("Controller", string.Empty)
                        + "/" + method.Name).ToLower();
                     
                    var httpMethod = HttpMethod.Get;

                    if (attribute != null)
                    {
                        httpMethod = attribute.Method;
                    }
                    
                    router.AddRoute(url, httpMethod, (request) => ExecuteAction(method, request, serviceCollection, controller)); 
                }
            }
        }

        private static HttpResponse ExecuteAction(MethodInfo method, HttpRequest httRequest, IServiceCollection serviceCollection, Type controller)
        {
            Controller controllerInstance = serviceCollection.CreateInstance(controller) as Controller;
            controllerInstance.Request = httRequest;
            var arguments = new List<object>();
            ParameterInfo[] parameters = method.GetParameters();

            foreach (ParameterInfo param in parameters)
            {
                var httpParamValue = GetParameterFromRequest(httRequest, param.Name);
                var parameterValue = Convert.ChangeType(httpParamValue, param.ParameterType);

                // Handle if the wanted argument is complex object(class)
                if (parameterValue == null && param.ParameterType != typeof(string))
                {
                    parameterValue = Activator.CreateInstance(param.ParameterType);
                    PropertyInfo[] properties = param.ParameterType.GetProperties();

                    foreach (PropertyInfo prop in properties)
                    {
                        var propertyHttpParamValue = GetParameterFromRequest(httRequest, prop.Name);
                        var propertyParamValue = Convert.ChangeType(propertyHttpParamValue, prop.PropertyType);
                        prop.SetValue(parameterValue, propertyParamValue);
                    }

                }

                arguments.Add(parameterValue);
            }

            var response = method.Invoke(controllerInstance, arguments.ToArray()) as HttpResponse;
            return response;
        }

        private static string GetParameterFromRequest(HttpRequest httpRequest, string parameterName)
        {
            parameterName = parameterName.ToLower();

            if (httpRequest.FormData.Any(x => x.Key.ToLower() == parameterName))
            {
                return httpRequest.FormData
                    .FirstOrDefault(x => x.Key.ToLower() == parameterName).Value;
            }

            if (httpRequest.QueryData.Any(x => x.Key.ToLower() == parameterName))
            {
                return httpRequest.QueryData
                    .FirstOrDefault(x => x.Key.ToLower() == parameterName).Value;
            }

            return null;
        }
    }
}
