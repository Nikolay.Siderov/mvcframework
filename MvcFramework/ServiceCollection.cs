﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebFramework.MvcFramework
{
    public class ServiceCollection : IServiceCollection
    {
        private Dictionary<Type, Type> container = new Dictionary<Type, Type>();

        public void Add<TSource, TDestination>()
        {
            container.Add(typeof(TSource), typeof(TDestination));
        }

        public object CreateInstance(Type type)
        {
            if (container.ContainsKey(type))
            {
                // If the dependency container have registered interface, replaces the type with the related service
                type = container[type];
            }

            // Gets the first constructor with less as possible parameters
            var constructor = type.GetConstructors()
                .OrderBy(x => x.GetParameters().Count())
                .FirstOrDefault();

            var parameters = constructor.GetParameters();
            var parameterValues = new List<object>();

            foreach (var parameter in parameters)
            {
                // Recursivly gets all types and their constructors with their typec etc...
                var parameterValue = CreateInstance(parameter.ParameterType);
                parameterValues.Add(parameterValue);
            }

            // Invokes the constructor with the new(replaced) services
            var obj = constructor.Invoke(parameterValues.ToArray());
            return obj;
        }
    }
}
