﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFramework.MvcFramework
{
    public interface IView
    {
        string ExecuteTemplate(object viewModel, string user);
    }
}
