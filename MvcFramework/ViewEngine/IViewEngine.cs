﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFramework.MvcFramework
{
    public interface IViewEngine
    {
        string GetHtml(string templateCode, object viewModel, string user = null);
    }
}
