﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebFramework.MvcFramework
{
    internal class ErrorView : IView
    {
        private IEnumerable<string> errors { get; set; }
        private string csharpCode { get; set; }

        public ErrorView(IEnumerable<string> errors, string csharpCode)
        {
            this.errors = errors;
            this.csharpCode = csharpCode;
        }

        public string ExecuteTemplate(object viewModel, string user)
        {
            var html = new StringBuilder();
            html.AppendLine($"<h1>View compile errors {this.errors.Count()}</h><ul>");

            foreach (var error in errors)
            {
                html.AppendLine($"<li>{error}</li>");
            }

            html.AppendLine($"</ul><pre>{csharpCode}</pre>");
            return html.ToString(); 
        }
    }
}
