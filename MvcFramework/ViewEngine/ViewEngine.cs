﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace WebFramework.MvcFramework
{
    public class ViewEngine : IViewEngine
    {
        public string GetHtml(string templateCode, object viewModel, string user = null)
        {
            var csharpCode = GenerateCSharmFromTemplate(templateCode, viewModel);
            IView executableObject = GenerateExecutableCode(csharpCode, viewModel);
            var html = executableObject.ExecuteTemplate(viewModel, user);
            return html;
        }

        private string GenerateCSharmFromTemplate(string templateCode, object viewModel)
        {
            string typeOfModel = "object";

            if (viewModel != null)
            {
                // This is needed since if we have List<int> the reflection GetType().FullName will return " List`1 "
                if (viewModel.GetType().IsGenericType)
                {
                    var modelName = viewModel.GetType().FullName;
                    var genericArguments = viewModel.GetType().GetGenericArguments();
                    typeOfModel = modelName.Substring(0, modelName.IndexOf('`'))
                        + "<" + string.Join(",", genericArguments.Select(x => x.FullName)) + ">";
                }
                else
                {
                    typeOfModel = viewModel.GetType().FullName;
                }
            }

            string csharpCode = @"
            using System;
            using System.Text;
            using System.Linq;
            using System.Collections.Generic;
            using WebFramework.MvcFramework;
            
            namespace ViewNameSpace
            {
                public class ViewClass : IView
                {
                    public string ExecuteTemplate(object viewModel, string user)
                    {
                        var Model = viewModel as "+ typeOfModel + @";
                        var User = user;
                        var html = new StringBuilder();
                        " + GetMethodBody(templateCode) + @"
                        return html.ToString();
                    }
                }
            }";

            return csharpCode;
        }

        private string GetMethodBody(string templateCode)
        {
            Regex csharpCodeRegex = new Regex(@"[^\""\s&\'\<]+");
            var supportedOperators = new List<string> { "for", "if", "else", "while", "foreach" };
            StringBuilder csharpCode = new StringBuilder();
            StringReader sr = new StringReader(templateCode);
            string line;

            while ((line = sr.ReadLine()) != null)
            {
                // Checks if the current line starts with supported operation for example: @for, @if, @while etc.
                if (supportedOperators.Any(x => line.TrimStart().StartsWith("@" + x)))
                {
                    var atSignLocation = line.IndexOf("@");
                    line = line.Remove(atSignLocation, 1);
                    csharpCode.AppendLine(line);
                } 
                else if (line.TrimStart().StartsWith("{") ||
                         line.TrimStart().StartsWith("}")) 
                {
                    csharpCode.AppendLine(line);
                }
                else
                {
                    csharpCode.Append($"html.AppendLine(@\"");

                    /** If line code contains the special char 
                     * transforms the inline code from "<li>@variable</li>" to => "<li>" + variable + "</li>"
                     */
                    while (line.Contains("@"))
                    {
                        var signLocation = line.IndexOf("@");
                        var htmlBeforeSign = line.Substring(0, signLocation);
                        csharpCode.Append(htmlBeforeSign.Replace("\"", "\"\"") + "\" + ");
                        var lineAfterSign = line.Substring(signLocation + 1);
                        var code = csharpCodeRegex.Match(lineAfterSign).Value;
                        csharpCode.Append(code + " + @\"");
                        line = lineAfterSign.Substring(code.Length);
                    }

                    csharpCode.AppendLine(line.Replace("\"", "\"\"") + "\");");
                }
            }
            
            return csharpCode.ToString();
        }
        /// <summary>
        /// Creates executable C# code from string
        /// </summary>
        /// <param name="csharpCode">C# Code as string</param>
        /// <param name="viewModel">Optional. The dynamically added data in the C# code, used to import dynamically the own assembly references in the compiler</param>
        /// <returns></returns>
        private IView GenerateExecutableCode(string csharpCode, object viewModel)
        {
            // Adds initial refferences and sets to .dll output
            var compileResult = CSharpCompilation.Create("ViewAssembly")
                .WithOptions(new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary))
                .AddReferences(MetadataReference.CreateFromFile(typeof(object).Assembly.Location))
                .AddReferences(MetadataReference.CreateFromFile(typeof(IView).Assembly.Location));

            if (viewModel != null)
            {
                var viewModelType = viewModel.GetType();
                var genericAgruments = viewModelType.GetGenericArguments();

                if (viewModelType.IsGenericType)
                {
                    // If the type of viewmodel is generics, gets all generics and adds their assemblies
                    foreach (var generic in genericAgruments)
                    {
                        compileResult = compileResult
                            .AddReferences(MetadataReference.CreateFromFile(generic.Assembly.Location));
                    }
                }
                else
                {
                    compileResult = compileResult
                        .AddReferences(MetadataReference.CreateFromFile(viewModelType.Assembly.Location));
                }
            }
            // Gets all libraries of .net standard
            var libraries = Assembly.Load(new AssemblyName("netstandard")).GetReferencedAssemblies();

            // Adds refference for earch library
            foreach (var library in libraries)
            {
                compileResult = compileResult
                    .AddReferences(MetadataReference.CreateFromFile(Assembly.Load(library).Location));
            }

            // Adds the C# code
            compileResult = compileResult.AddSyntaxTrees(SyntaxFactory.ParseSyntaxTree(csharpCode));

            using MemoryStream memoryStream = new MemoryStream();
            // Writes the data in the memory stream
            var result = compileResult.Emit(memoryStream);

            if (!result.Success)
            {
                // Returns ErrorView with all error messages and the code
                return new ErrorView(result.Diagnostics
                    .Where(x => x.Severity == DiagnosticSeverity.Error)
                    .Select(x => x.GetMessage()),
                    csharpCode);
            }

            try
            {
                // Sets the position of the ms to initial
                memoryStream.Seek(0, SeekOrigin.Begin);
                var byteAssembly = memoryStream.ToArray();
                var assembly = Assembly.Load(byteAssembly);
                // Gets the Type of the ViewClass
                var viewType = assembly.GetType("ViewNameSpace.ViewClass");
                // Creates instance
                var instance = Activator.CreateInstance(viewType) as IView;
                return instance;
            }
            catch (Exception ex)
            {
                return new ErrorView(new List<string>() { ex.ToString() }, csharpCode);
            }  
        }
    }
}
