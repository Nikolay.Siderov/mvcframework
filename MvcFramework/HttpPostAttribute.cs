﻿using System;
using System.Collections.Generic;
using System.Text;
using WebFramework.HttpServer;

namespace WebFramework.MvcFramework
{
    public class HttpPostAttribute : BaseHttpAttribute
    {
        public HttpPostAttribute() { }

        public HttpPostAttribute(string url)
        {
            this.Url = url;
        }
        public override HttpMethod Method => HttpMethod.Post;
    }
}
