﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using WebFramework.HttpServer;

namespace WebFramework.MvcFramework
{

    public abstract class Controller
    {
        private ViewEngine viewEngine = new ViewEngine();
        public  HttpRequest Request { get; set; }

        private const string UserIdSessionName = "UserId";

        protected HttpResponse View(object viewModel = null, [CallerMemberName] string viewPath = null)
        {
            string controllerName = this.GetType().Name.Replace("Controller", string.Empty);
            string controllerView = File.ReadAllText("Views" + Path.DirectorySeparatorChar + controllerName + Path.DirectorySeparatorChar + viewPath + ".html");
            string renderedHtml = PutViewInLayout(controllerView, viewModel, this.GetUserId());
            return new HttpResponse(renderedHtml);
        }

        protected HttpResponse Redirect(string url)
        {
            HttpResponse response = new HttpResponse();
            response.Headers.Add(new Header("Location", url));
            response.Status = StatusCode.Found;
            return response;
        }

        protected HttpResponse Error(string errorMessage, StatusCode statusCode = StatusCode.Forbidden)
        {
            string message = @"<div class='alert alert-danger' role='alert'>" + errorMessage + "</div>";
            string renderedHtml = PutViewInLayout(message, null, GetUserId());
            return new HttpResponse(renderedHtml, statusCode);
        }

        protected void SignIn(string userId)
        {
            Request.Session[UserIdSessionName] = userId;
        }

        protected void SignOut()
        { 
            if (IsUserSignedIn())
            {
                Request.Session[UserIdSessionName] = null;
            }
        }

        protected bool IsUserSignedIn() =>
            Request.Session.ContainsKey(UserIdSessionName) &&
            Request.Session[UserIdSessionName] != null;

        protected string GetUserId()
        {
            return IsUserSignedIn() 
                ? Request.Session[UserIdSessionName]
                : null;
        }

        private string PutViewInLayout(string controllerView, object viewModel = null, string user = null)
        {
            string layout = File.ReadAllText("Views" + Path.DirectorySeparatorChar + "_Layout.html");
            string renderedLayout = viewEngine.GetHtml(layout, viewModel, user);
            string renderedControllerView = viewEngine.GetHtml(controllerView, viewModel, user);
            string renderedHtml = renderedLayout.Replace("_VIEW_GOES_HERE_", renderedControllerView);
            return renderedHtml;
        }

    }
}
