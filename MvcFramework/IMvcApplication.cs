﻿using System;
using System.Collections.Generic;
using WebFramework.HttpServer;

namespace WebFramework.MvcFramework
{
    public interface IMvcApplication
    {
        void ConfigureServices(IServiceCollection serviceCollection);
        void Configure(IHttpServer server);
    }
}
