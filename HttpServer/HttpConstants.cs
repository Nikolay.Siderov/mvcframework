﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFramework.HttpServer
{
    internal static class HttpConstants
    {
        public const int BufferSize = 4096;
        public const string NewLine = "\r\n";
        public const string RequestCookieHeader = "Cookie";
        public const string CookieSession = "sid";
        public const string SetCookie = "Set-cookie";
    }
}
