﻿namespace WebFramework.HttpServer
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Linq;
    using System.Net;

    public class HttpRequest
    {
        public HttpRequest()
        {
        }

        public HttpRequest(string requestString)
        {
            GetHeadersData(requestString);
        }

        public HttpMethod Method { get; private set; }
        public string Path { get; private set; } = string.Empty;
        public ICollection<Cookie> Cookies { get; private set; } = new List<Cookie>();
        public ICollection<Header> Headers { get; private set; } = new List<Header>();
        public string Body { get; private set; }
        public Dictionary<string, string> FormData { get; set; } = new Dictionary<string, string>();
        public Dictionary<string, string> QueryData { get; set; } = new Dictionary<string, string>();

        public Dictionary<string, string> Session;

        /** Contains all sessions with the following structure
         * - SID (Session Id)
         * -- Key -> Value (Data for current SID)
         */
        public static IDictionary<string, Dictionary<string, string>> Sessions = new Dictionary<string, Dictionary<string, string>>();

        private void GetHeadersData(string stringData)
        {
            List<string> lines = SplitToLines(stringData);

            string headerLine = lines[0];
            
            string[] headerLineParts = headerLine.Split(' ');

            Method = (HttpMethod)Enum.Parse(typeof(HttpMethod), headerLineParts[0], true);
            Path = headerLineParts[1];

            bool isInHeaders = true;

            StringBuilder bodyBuilder = new StringBuilder();

            for (int i = 1; i < lines.Count; i++)
            {

                if (string.IsNullOrWhiteSpace(lines[i]))
                {
                    isInHeaders = false;
                    continue;
                }

                if (isInHeaders)
                {
                    Headers.Add(new Header(lines[i]));
                }
                else
                {
                    var line = lines[i];
                    bodyBuilder.AppendLine(line);
                }
            }

            Body = bodyBuilder.ToString().TrimEnd('\r', '\n');
            SplitParameters(Body, FormData);

            var queryString = ProccessQueryString();
            SplitParameters(queryString, QueryData);

            ProccessCookies();
            ProccessSessionCookies();
        }

        private static void SplitParameters(string parametersAsString, Dictionary<string, string> store)
        {
            var parameters = parametersAsString.Split(new[] { "&" }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string parameter in parameters)
            {
                string[] parameterParts = parameter.Split('=');
                string name = parameterParts[0];
                string value = WebUtility.UrlDecode(parameterParts[1]);
                if (!store.ContainsKey(name))
                {
                    store.Add(name, value);
                }
            }
        }

        private string ProccessQueryString()
        {
            string queryString;

            if (Path.Contains("?"))
            {
                var parts = Path.Split(new char[] { '?' }, 2);
                Path = parts[0];
                queryString = parts[1];
            }
            else
            {
                queryString = string.Empty;
            }

            return queryString;
        }
        
        private void ProccessCookies()
        {
            if (Headers.Any(x => x.Name == HttpConstants.RequestCookieHeader))
            {
                string cookiesAsString = Headers.FirstOrDefault(x => x.Name == HttpConstants.RequestCookieHeader).Value;
                string[] cookies = cookiesAsString.Split(new string[] { "; " }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var cookieString in cookies)
                {
                    Cookies.Add(new Cookie(cookieString));
                }
            }
        }

        private void ProccessSessionCookies()
        {
            var sessionCookie = Cookies.FirstOrDefault(x => x.Name == HttpConstants.CookieSession);

            if (sessionCookie == null)
            {
                var sessionId = Guid.NewGuid().ToString();
                Session = new Dictionary<string, string>();
                Sessions.Add(sessionId, Session);
                Cookies.Add(new Cookie(HttpConstants.CookieSession, sessionId));
            }
            else if (!Sessions.ContainsKey(sessionCookie.Value))
            {
                Session = new Dictionary<string, string>();
                Sessions.Add(sessionCookie.Value, Session);
            }
            else
            {
                this.Session = Sessions[sessionCookie.Value];
            }
        }

        private List<string> SplitToLines(string input)
        {
            using StringReader reader = new StringReader(input);
            string line;
            var store = new List<string>();

            while ((line = reader.ReadLine()) != null)
            {
                store.Add(line);
            }

            return store;
        }
    }
}