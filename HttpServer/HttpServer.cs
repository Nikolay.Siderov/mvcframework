﻿namespace WebFramework.HttpServer
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading.Tasks;

    public class HttpServer : IHttpServer, IRouter
    {
        private IDictionary<string, List<Dictionary<HttpMethod, Func<HttpRequest, HttpResponse>>>> routeTable = new Dictionary<string, List<Dictionary<HttpMethod, Func<HttpRequest, HttpResponse>>>>();
        private string staticDir = Path.Combine(Environment.CurrentDirectory, "wwwroot") + Path.DirectorySeparatorChar;

        public async Task StartAsync(int port)
        {
            TcpListener tcpListener = new TcpListener(IPAddress.Loopback, port);
            tcpListener.Start();
            Console.Write($"Server is listening on {IPAddress.Loopback}:{port}...");

            while (true)
            {
                var tcpClient = await tcpListener.AcceptTcpClientAsync();
                ProcessTcpClientAsync(tcpClient);
            }
        }

        public void AddRoute(string route, HttpMethod method, Func<HttpRequest, HttpResponse> action)
        {
            var methodAction = new Dictionary<HttpMethod, Func<HttpRequest, HttpResponse>>();
            methodAction.Add(method, action);
            route = route.ToLower();

            if (routeTable.ContainsKey(route))
            {
                routeTable[route].Add(methodAction);
            }
            else
            {
                var actionsList = new List<Dictionary<HttpMethod, Func<HttpRequest, HttpResponse>>>();
                actionsList.Add(methodAction);
                routeTable.Add(route, actionsList);
            }
        }

        public void UseStatic(string staticFolderName)
        {
            staticDir = Path.Combine(Environment.CurrentDirectory, staticFolderName) + Path.DirectorySeparatorChar;
        }

        private async Task ProcessTcpClientAsync(TcpClient tcpClient)
        {
            using NetworkStream stream = tcpClient.GetStream();

            byte[] data = await ReadStreamAsync(stream);
            string stringData = Encoding.UTF8.GetString(data);
            HttpRequest httpRequest = new HttpRequest();

            if (!string.IsNullOrEmpty(stringData))
            {
                httpRequest = new HttpRequest(stringData);
                Console.WriteLine($"{httpRequest.Method.ToString().ToUpper()} request for {httpRequest.Path}");
            }

            ProccessHttpRequest(httpRequest, out HttpResponse httpResponse);

            byte[] headerBytes = httpResponse.GetHeaders();
            await stream.WriteAsync(headerBytes, 0, headerBytes.Length);
            await stream.WriteAsync(httpResponse.Body, 0, httpResponse.Body.Length);
        }

        private HttpResponse ProccessHttpRequest(HttpRequest httpRequest, out HttpResponse httpResponse)
        {
            string path = httpRequest.Path.ToLower();

            if (string.IsNullOrEmpty(path))
            {
                httpResponse = new HttpResponse(StatusCode.NotFound);
                return httpResponse;
            }
            
            if (routeTable.ContainsKey(path))
            {
                var navigatedRoute = routeTable[path];
                var action = navigatedRoute.Where(x => x.ContainsKey(httpRequest.Method)).FirstOrDefault();

                if (action != null)
                {
                    httpResponse = action.FirstOrDefault().Value.Invoke(httpRequest);
                }
                else
                {
                    httpResponse = new HttpResponse(StatusCode.NotFound);
                }
            }
            else // If request path it's not contained in the route table probably the request is for static files
            {  
                var fileName = httpRequest.Path.Substring(1, httpRequest.Path.Length - 1);
                string filePath = (staticDir + fileName).Replace(@"/", Path.DirectorySeparatorChar.ToString());

                bool fileExist = File.Exists(filePath);

                if (fileExist)
                {
                    var fileExtenstion = new FileInfo(fileName).Extension;
                    var mimeType = GetMimeType(fileExtenstion);
                    byte[] file = File.ReadAllBytes(filePath);
                    httpResponse = new HttpResponse(mimeType, file);
                }
                else
                {
                    httpResponse = new HttpResponse(StatusCode.NotFound);
                }
            }

            var sessionCookie = httpRequest.Cookies.FirstOrDefault(x => x.Name == HttpConstants.CookieSession);

            if (sessionCookie != null)
            {
                var responseSessionCookie = new ResponseCookie(sessionCookie.Name, sessionCookie.Value);
                responseSessionCookie.Path = "/";
                httpResponse.Cookies.Add(responseSessionCookie);
            }

            return httpResponse;
        }

        private string GetMimeType(string fileName)
        {
            return fileName switch
            {
                ".jpg" => ContentType.Jpeg,
                ".ico" => ContentType.Favicon,
                ".js" => ContentType.JavaScript,
                ".css" => ContentType.Style,
                _ => ContentType.Html
            };
        }

        private async Task<byte[]> ReadStreamAsync(Stream stream)
        {
            byte[] buffer = new byte[HttpConstants.BufferSize];
            List<byte> data = new List<byte>();

            while (true)
            {
                int readedBytes = await stream.ReadAsync(buffer, 0, HttpConstants.BufferSize);

                if (readedBytes < buffer.Length)
                {
                    byte[] partialBytes = new byte[readedBytes];
                    Array.Copy(buffer, partialBytes, readedBytes);
                    data.AddRange(partialBytes);
                    break;
                }
                else
                {
                    data.AddRange(buffer);
                }
            }
            return data.ToArray();
        }
    }
}
