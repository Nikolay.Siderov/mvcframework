﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebFramework.HttpServer
{
    public interface IHttpServer : IRouter
    {
        Task StartAsync(int port);
        void UseStatic(string staticFolderName);
    }
}
