﻿using System.Text;

namespace WebFramework.HttpServer
{
    public class ResponseCookie : Cookie
    {
        public ResponseCookie(string cookie) : base(cookie)
        {
        }

        public ResponseCookie(string name, string value) : base(name, value)
        {
        }


        public int MaxAge { get; set; }
        public string Path { get; set; } = "/";
        public bool HttpOnly { get; set; }
        public bool Secure { get; set; }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append($"{Name}={Value}; Path={Path};");

            if (MaxAge != 0)
            {
                builder.Append($" Max-Age={MaxAge};");
            }

            if (HttpOnly)
            {
                builder.Append($" HttpOnly;");
            }

            if (Secure)
            {
                builder.Append($" Secure;");
            }

            return builder.ToString();
        }
    }
}
