﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFramework.HttpServer
{
    public class HttpResponse
    {
        public HttpResponse()
        {
        }

        public HttpResponse(
            string body,
            StatusCode statusCode = StatusCode.Ok)
        {
            Status = statusCode;
            Body = Encoding.UTF8.GetBytes(body);
            Headers.Add(new Header("Content-Type: " + ContentType.Html));
            Headers.Add(new Header("Content-Length: " + Body.Length));
        }

        public HttpResponse(
            string contentType,
            byte[] body,
            StatusCode statusCode = StatusCode.Ok)
        {
            Status = statusCode;
            Body = body;
            Headers.Add(new Header("Content-Type: " + contentType));
            Headers.Add(new Header("Content-Length: " + Body.Length));
        }

        public HttpResponse(StatusCode statusCode)
        {
            Status = statusCode;
        }

        public byte[] GetHeaders()
        {
            StringBuilder builder = new StringBuilder();
            var firstLine = $"HTTP/1.1 {(int)Status} {Status.ToString().ToUpper()}" + HttpConstants.NewLine;
            builder.Append(firstLine);

            foreach (var header in Headers)
            {
                builder.Append(header.ToString() + HttpConstants.NewLine);
            }

            foreach (var cookie in Cookies)
            {
                builder.Append($"{HttpConstants.SetCookie}: " + cookie.ToString() + HttpConstants.NewLine);
            }

            builder.Append(HttpConstants.NewLine);
            return Encoding.UTF8.GetBytes(builder.ToString());
        }

        public StatusCode Status { get; set; } = StatusCode.Ok;
        public ICollection<Header> Headers { get; set; } = new List<Header>();
        public ICollection<ResponseCookie> Cookies { get; set; } = new List<ResponseCookie>();
        public byte[] Body { get; set; } = new byte[0];
    }
}