﻿using System;

namespace WebFramework.HttpServer
{
    public interface IRouter
    {
        void AddRoute(string route, HttpMethod method, Func<HttpRequest, HttpResponse> action);
    }
}
