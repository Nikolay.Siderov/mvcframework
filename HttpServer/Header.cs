﻿using System;

namespace WebFramework.HttpServer
{
    public class Header
    {
        public Header(string stringData)
        {
            string[] parts = stringData.Split(new string[] { ": " }, 2, StringSplitOptions.None);
            Name = parts[0].Trim();
            Value = parts[1].Trim();
        }

        public Header(string name, string value)
        {
            Name = name;
            Value = value;
        }

        public string Name { get; private set; }
        public string Value { get; private set; }

        public override string ToString()
        {
            return $"{Name}: {Value}";
        }
    }
}
