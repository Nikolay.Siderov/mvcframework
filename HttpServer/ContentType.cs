﻿namespace WebFramework.HttpServer
{
    internal static class ContentType
    {
        public const string Html = "text/html";
        public static string Favicon = "image/ico";
        public static string Jpeg = "image/jpg";
        public static string JavaScript = "text/javascript";
        public static string Style = "text/css";
    }
}
