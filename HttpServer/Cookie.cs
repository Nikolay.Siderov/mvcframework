﻿namespace WebFramework.HttpServer
{
    public class Cookie
    {
        public Cookie(string cookie)
        {
            string[] cookieParts = cookie.Split(new char[] { '=' }, 2);
            Name = cookieParts[0];
            Value = cookieParts[1];
        }

        public Cookie(string name, string value)
        {
            Name = name;
            Value = value;
        }

        public string Name { get; set; }
        public string Value { get; set; }

    }
}