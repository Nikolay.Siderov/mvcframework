﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebFramework.DemoApp.Data;
using WebFramework.DemoApp.ViewModels;
using WebFramework.HttpServer;
using WebFramework.MvcFramework;

namespace WebFramework.DemoApp.Controllers
{
    class CardsController : Controller
    {
        private ApplicationDbContext dbContext;
        public CardsController(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public HttpResponse Add()
        {
            return View();
        }

        [HttpPost("/cards/add")]
        public HttpResponse OnAdd(AddCardInputModel model)
        {
            var card = new Card()
            {
                Name = model.Name,
                ImageUrl = model.Image,
                Keyword = model.Keyword,
                Attack = model.Attack,
                Health = model.Health,
                Description = model.Description,
                CreatedOn = DateTime.UtcNow
            };

            var dbContext = new ApplicationDbContext();
            dbContext.Set<Card>().Add(card);
            dbContext.SaveChanges();
            return Redirect("/");
        }

        public HttpResponse Collection()
        {
            var dbContext = new ApplicationDbContext();

            List<Card> cards = dbContext.Set<Card>().ToList();
            return View(cards);
        }
    }
}
