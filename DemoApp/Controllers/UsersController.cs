﻿using System;
using WebFramework.DemoApp.Data;
using WebFramework.DemoApp.Services;
using WebFramework.HttpServer;
using WebFramework.MvcFramework;

namespace WebFramework.DemoApp.Controllers
{
    public class UsersController : Controller
    {
        private readonly IUserService userService;

        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }

        public HttpResponse Login()
        {
            return View();
        }

        [HttpGet("/logout")]
        public HttpResponse Logout()
        {
            SignOut();
            return Redirect("/");
        }

        [HttpPost("/users/login")]
        public HttpResponse OnLogin(string username, string password)
        {
            var userId = userService.GetUserId(username, password);

            if (string.IsNullOrEmpty(userId))
            {
                return Error("Wrong username/password");
            }

            SignIn(userId);
            return Redirect("/cards/collection"); 
        }

        public HttpResponse Register()
        {
            return View();
        }

        [HttpPost("/users/register")]
        public HttpResponse OnRegister(string username, string password, string email)
        {
            var isUserCreated = userService.CreateUser(username, password, email);
            
            if (isUserCreated)
            {
                return Redirect("/");
            }
            else
            {
                return Error("Username or password not available ");
            }

        }
    }
}
