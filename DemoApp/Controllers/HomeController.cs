﻿using WebFramework.DemoApp.ViewModels;
using WebFramework.HttpServer;
using WebFramework.MvcFramework;

namespace WebFramework.DemoApp.Controllers
{
    public class HomeController : Controller
    {

        [HttpGet("/")]
        public HttpResponse Index()
        {
            if (IsUserSignedIn())
            {
                return Redirect("/cards/collection");
            }
            else
            {
                var viewModel = new HomeViewModel();
                return View(viewModel);
            }
        }

        [HttpGet]
        public HttpResponse About()
        {
            return View();
        }
    }
}
