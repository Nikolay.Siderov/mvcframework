﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebFramework.DemoApp.Data;
using WebFramework.MvcFramework;

namespace WebFramework.DemoApp.Services
{
    class UserService : IUserService
    {
        private DbContext dbContext { get; set; }

        public UserService(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public bool CreateUser(string username, string password, string email)
        {
            if (IsUserNameAvailable(username) && IsEmailAvailable(email))
            {
                var user = new User
                {
                    Username = username,
                    Password = ComputeHash(password),
                    Email = email,
                    Role = IdentityRole.User
                };

                dbContext.Set<User>().Add(user);
                dbContext.SaveChanges();
                return true;
            }
            return false;
        }

        public string GetUserId(string username, string password)
        {
            var user = dbContext.Set<User>().FirstOrDefault(x => x.Username.Equals(username));
            if (user != null && user.Password.Equals(ComputeHash(password)))
            {
                return user.Id.ToString();
            }
            else
            {
                return null;
            }
        }

        public bool IsEmailAvailable(string email) => !this.dbContext.Set<User>().Any(x => x.Email.Equals(email));

        public bool IsUserNameAvailable(string username) => !this.dbContext.Set<User>().Any(x => x.Username.Equals(username));

        private static string ComputeHash(string input)
        {
            var bytes = Encoding.UTF8.GetBytes(input);
            using var hash = System.Security.Cryptography.SHA512.Create();

            var hashedInputBytes = hash.ComputeHash(bytes);

            var hashedInputStringBuilder = new StringBuilder(128);
            foreach (var b in hashedInputBytes)
                hashedInputStringBuilder.Append(b.ToString("X2"));
            return hashedInputStringBuilder.ToString();

        }
    }
}
