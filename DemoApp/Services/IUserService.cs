﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFramework.DemoApp.Services
{
    public interface IUserService
    {
        bool CreateUser(string username, string password, string email);
        string GetUserId(string username, string password);
    }
}
