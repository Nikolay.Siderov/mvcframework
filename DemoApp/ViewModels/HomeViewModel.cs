﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFramework.DemoApp.ViewModels
{
    public class HomeViewModel
    {
        public int CurrentYear { get; set; }
        public string Message { get; set; }
    }
}
