﻿namespace WebFramework.DemoApp
{
    using System;
    using System.Threading.Tasks;
    using WebFramework.MvcFramework;

    class Program
    {
        static async Task Main(string[] args)
        {
            await Host.StartUpAsync(new Startup(), 80);
        }
    }
}
