﻿using Microsoft.EntityFrameworkCore;
using System;
using WebFramework.DemoApp.Data;
using WebFramework.DemoApp.Services;
using WebFramework.HttpServer;
using WebFramework.MvcFramework;

namespace WebFramework.DemoApp
{
    public class Startup : IMvcApplication
    {
        public void Configure(IHttpServer server)
        {
            new ApplicationDbContext().Database.Migrate();

        }

        public void ConfigureServices(IServiceCollection serviceCollection)
        {
            serviceCollection.Add<IUserService, UserService>();
            serviceCollection.Add<ICardsService, CardsService>();
        }
    }
}
