﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WebFramework.MvcFramework;

namespace WebFramework.DemoApp.Data
{
    public class User : IdentityUser<int>
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public IdentityRole Role { get; set; }

        public virtual ICollection<UserCard> Cards { get; set; } = new HashSet<UserCard>();
    }
}
